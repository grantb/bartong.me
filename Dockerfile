FROM debian:stable-slim
MAINTAINER grantb@member.fsf.org

RUN apt update && apt install -y python3 python3-pip && rm -rf /var/lib/apt/lists

COPY app/ /opt/bartong/

RUN pip3 install -r /opt/bartong/requirements.txt

WORKDIR /opt/bartong/

CMD gunicorn -w 4 --bind 0.0.0.0:8080 wsgi

