#!/usr/bin/env python3

# bartong.py
# Grant Barton 2019

import json
import time
from datetime import datetime
from calendar import day_name
from os import getenv
from sys import stderr

#from dotenv import load_dotenv
import ipinfo
import requests
from flask import Flask, render_template, request

application = Flask(__name__, template_folder='.')

def read_weather(data):
    summary = json.loads(data)["currently"]["summary"]
    now_temp = int(json.loads(data)["currently"]["temperature"])

    daily_weather = []

    for x in range(0, 5):
        arr = []
        # date:
        unixtime = json.loads(data)["daily"]["data"][x]["time"]
        arr.append(time.strftime("%D %H:%M", time.localtime(int(unixtime))).split()[0])
        # summary:
        arr.append(json.loads(data)["daily"]["data"][x]["summary"])
        # precipitation chance:
        arr.append(int(float(json.loads(data)["daily"]["data"][x]["precipProbability"])*100))
        # high temp:
        arr.append(int(json.loads(data)["daily"]["data"][x]["temperatureHigh"]))
        # low temp:
        arr.append(int(json.loads(data)["daily"]["data"][x]["temperatureLow"]))
        daily_weather.append(arr)

    output = summary, now_temp, daily_weather
    return output

@application.route('/')
def homepage():
    #load_dotenv()
    DARKSKY_TOKEN = getenv('DARKSKY_TOKEN')
    IPINFO_TOKEN = getenv('IPINFO_TOKEN')

    try:
        with open("/app/weather.json", "r") as readfile:
            data = readfile.read()
            bham_output = read_weather(data)
            bham_summary = bham_output[0]
            bham_now_temp = bham_output[1]
            bham_daily_weather = bham_output[2]
    except Exception as e:
        # fill variables with placeholders if the API call fails
        bham_summary = "Not Found"
        bham_now_temp = "Not Found"
        bham_daily_weather = []

        for x in range(0, 5):
            arr = []
            for i in range(0, 5):
                arr.append("--")

            bham_daily_weather.append(arr)

    # IPINFO call to get longitude and latitude:
    client_ip = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
    handler = ipinfo.getHandler(IPINFO_TOKEN)
    details = handler.getDetails(client_ip)

    latitude = details.latitude
    longitude = details.longitude

    r = requests.get("https://api.darksky.net/forecast/{}/{},{}?exclude=minutely,hourly,alerts,flags".format(DARKSKY_TOKEN, latitude, longitude))
    if r.status_code == 200:
        your_output = read_weather(r.text)
        your_summary = your_output[0]
        your_now_temp = your_output[1]
        your_daily_weather = your_output[2]
    else:
        stderr.write("Error: Response code {} on Darksky API call".format(r.status_code))
        your_now_temp = "Not found"
        your_summary = "Not found"
        your_daily_weather = []
        for x in range(0, 5):
            arr = []
            for i in range(0, 5):
                arr.append("--")

            your_daily_weather.append(arr)
        
    return render_template("index.html", bham_summary=bham_summary, bham_temp=bham_now_temp, bham_daily=bham_daily_weather, your_summary=your_summary, your_temp=your_now_temp, your_daily=your_daily_weather)

if __name__ == '__main__':
    application.run(host='0.0.0.0', debug=False)
