#!/usr/bin/env python3

from bartong import application

if __name__ == "__main__":
    application.run()
