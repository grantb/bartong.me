#!/usr/bin/env python3

import requests
from os import getenv
from dotenv import load_dotenv
from time import sleep

def get_weather():
    load_dotenv(verbose=True)
    api_key = getenv('DARKSKY_TOKEN')
    r = requests.get("https://api.darksky.net/forecast/" + api_key + "/48.7519,-122.4786?exclude=minutely,hourly,alerts,flags")

    if r.status_code != 200:
        print("Whoops")
        print(r.status_code)

    else:
        with open("/tmp/bartong/weather.json", "w") as writefile:
            writefile.write(r.text)

def main():
    while True:
        get_weather()
        sleep(1200)

main()
